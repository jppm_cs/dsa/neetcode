class HashTable:
    def __init__(self):
        self.hash_table_dimension = 11
        self.next_prime_dimension = self.next_prime(self.hash_table_dimension)
        self.hash_table = [-1] * self.hash_table_dimension
        # [[], [], [], [], [], [], [], [], [], []]
        # 0   1   2   3   4   5   6   7   8   9

    def next_prime(self, hash_table_dimension):
        if self.is_prime(hash_table_dimension):
            print(hash_table_dimension)
            return hash_table_dimension

        else:
            while not self.is_prime(hash_table_dimension):
                hash_table_dimension += 1
            print(hash_table_dimension)
            return hash_table_dimension

    def is_prime(self, number):
        for i in range(2, number):
            if number % i == 0:
                return False
        return True

    def hash_function(self, value):
        # usar siguiente primo para evitar colisiones
        # retorna index o key
        return value % self.next_prime_dimension

    def insert_value(self, value):
        index = self.hash_function(value)
        self.hash_table[index]=value

    def remove_value(self, value):
        index = self.hash_function(value)
        self.hash_table[index]=-1
    def get_value(self,value):
        index = self.hash_function(value)
        return self.hash_table[index]
    def show_table(self):
        print(self.hash_table)


hash_table = HashTable()
hash_table.insert_value(10)
hash_table.insert_value(20)
hash_table.insert_value(30)
hash_table.insert_value(40)
hash_table.remove_value(40)
hash_table.show_table()

