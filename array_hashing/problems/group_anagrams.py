def group_anagrams(strs):
    hash_map = {}
    for element in strs:
        sort_element = "".join(sorted(element))
        if element not in hash_map:
            hash_map[sort_element]=[element]
        else:
            hash_map[sort_element].append(element)
    final_list=[]
    for value in hash_map.values():
        final_list.append(value)
    return final_list


if __name__ == "__main__":
    print(group_anagrams(["eat", "tea", "tan", "ate", "nat", "bat"]))
