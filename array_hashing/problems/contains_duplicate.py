def contain_duplicates(nums):
    return len(nums) != len(set(nums))


if __name__ == "__main__":
    print(contain_duplicates([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]))
