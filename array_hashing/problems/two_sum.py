# def twoSum(nums,target):
#     lista_as_set = {k:i for (i,k) in enumerate(nums)}
#     print(lista_as_set)
#     for key in lista_as_set.keys():
#         my_target = target - key
#         if my_target in lista_as_set.keys() and my_target != key:
#             second_key = lista_as_set[my_target]
#             return [lista_as_set[key],second_key]


def two_sum(nums, target):
    hash_map_num = dict()
    for index in range(0, len(nums)):
        new_target = target - nums[index]
        if new_target in hash_map_num:
            return [hash_map_num[new_target], index]
        hash_map_num[nums[index]] = index


if __name__ == "__main__":
    print(two_sum([3, 3], 6))
