def is_anagram(s, t):
    if len(s) != len(t):
        return False
    hash_set_s = {}
    hash_set_t = {}
    for index in range(0, len(s)):
        if s[index] not in hash_set_s:
            hash_set_s[s[index]] = 1
        else:
            new_value = hash_set_s[s[index]] + 1
            hash_set_s.update({s[index]: new_value})
        if t[index] not in hash_set_t:
            hash_set_t[t[index]] = 1
        else:
            new_value = hash_set_t[t[index]] + 1
            hash_set_t.update({t[index]: new_value})
    for key in hash_set_s:
        if key not in hash_set_t:
            return False
        if hash_set_s[key]!=hash_set_t[key]
            return False
    return True



if __name__ == "__main__":
    is_anagram("hola", "olha")
